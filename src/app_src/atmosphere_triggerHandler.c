#include "atmosphere_triggerHandler.h"
#include "atmosphere_abilityHandler.h"

#ifdef __cplusplus
	extern "C"{
#endif

void ATMO_TriggerHandler(unsigned int triggerHandleId, ATMO_Value_t *value) {
	switch(triggerHandleId) {
		case ATMO_TRIGGER(BLEConnection, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(BLEConnection, connected):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedIconLinesDisplay, displayPage), value);
			break;
		}

		case ATMO_TRIGGER(BLEConnection, disconnected):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedSystemStatusDisplay, displayPage), value);
			break;
		}

		case ATMO_TRIGGER(BLEConnection, pairingRequested):
		{
			break;
		}

		case ATMO_TRIGGER(BLEConnection, pairingSucceeded):
		{
			break;
		}

		case ATMO_TRIGGER(BLEConnection, pairingFailed):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedSystemStatusDisplay, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedSystemStatusDisplay, onDisplayed):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, navigateUp):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, navigateDown):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, navigateLeft):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, navigateRight):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, processTopRightButton):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, processBottomRightButton):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, processTopLeftButton):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedPageController, processBottomLeftButton):
		{
			break;
		}

		case ATMO_TRIGGER(SX9500Touch, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(SX9500Touch, touchDataRead):
		{
			break;
		}

		case ATMO_TRIGGER(SX9500Touch, upPressed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, navigateUp), value);
			break;
		}

		case ATMO_TRIGGER(SX9500Touch, downPressed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, navigateDown), value);
			break;
		}

		case ATMO_TRIGGER(SX9500Touch, leftPressed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, navigateLeft), value);
			break;
		}

		case ATMO_TRIGGER(SX9500Touch, rightPressed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, navigateRight), value);
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicFireClass, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicFireClass, written):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine1Text), value);
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicFireClass, subscibed):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicFireClass, unsubscribed):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedIconLinesDisplay, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedIconLinesDisplay, onDisplayed):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedIconLinesDisplay, onLeave):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedIconLinesDisplay, topRightButtonPressed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, toggleRed), value);
			break;
		}

		case ATMO_TRIGGER(EmbeddedIconLinesDisplay, bottomRightButtonPressed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessHigh), value);
			break;
		}

		case ATMO_TRIGGER(EmbeddedIconLinesDisplay, topLeftButtonPressed):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedIconLinesDisplay, bottomLeftButtonPressed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setOff), value);
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, topRightPushed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, processTopRightButton), value);
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, bottomRightPushed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, processBottomRightButton), value);
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, topLeftPushed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, displayRootPage), value);
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, bottomLeftPushed):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedPageController, processBottomLeftButton), value);
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicAgent, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicAgent, written):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine2Text), value);
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicAgent, subscibed):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicAgent, unsubscribed):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicCrew, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicCrew, written):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine3Text), value);
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicCrew, subscibed):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicCrew, unsubscribed):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicColour, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicColour, written):
		{
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine4Text), value);
			ATMO_AbilityHandler(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setColorHex), value);
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicColour, subscibed):
		{
			break;
		}

		case ATMO_TRIGGER(BLECharacteristicColour, unsubscribed):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, triggered):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, brightnessSet):
		{
			break;
		}

		case ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, colorSet):
		{
			break;
		}

	}

}

#ifdef __cplusplus
}
#endif
