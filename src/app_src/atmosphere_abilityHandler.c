#include "atmosphere_abilityHandler.h"
#include "atmosphere_triggerHandler.h"

#ifdef __cplusplus
	extern "C"{
#endif

void ATMO_AbilityHandler(unsigned int abilityHandleId, ATMO_Value_t *value) {
	switch(abilityHandleId) {
		case ATMO_ABILITY(BLEConnection, trigger):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_trigger(value, &BLEConnection_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLEConnection, triggered), &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(BLEConnection, setup):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_setup(value, &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(BLEConnection, disconnect):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_disconnect(value, &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(BLEConnection, connected):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_connected(value, &BLEConnection_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLEConnection, connected), &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(BLEConnection, disconnected):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_disconnected(value, &BLEConnection_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLEConnection, disconnected), &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(BLEConnection, pairingRequested):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_pairingRequested(value, &BLEConnection_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLEConnection, pairingRequested), &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(BLEConnection, pairingSucceeded):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_pairingSucceeded(value, &BLEConnection_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLEConnection, pairingSucceeded), &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(BLEConnection, pairingFailed):
		{
			ATMO_Value_t BLEConnection_Value;
			ATMO_InitValue(&BLEConnection_Value);
			BLEConnection_pairingFailed(value, &BLEConnection_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLEConnection, pairingFailed), &BLEConnection_Value);
			ATMO_FreeValue(&BLEConnection_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedSystemStatusDisplay, trigger):
		{
			ATMO_Value_t EmbeddedSystemStatusDisplay_Value;
			ATMO_InitValue(&EmbeddedSystemStatusDisplay_Value);
			EmbeddedSystemStatusDisplay_trigger(value, &EmbeddedSystemStatusDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedSystemStatusDisplay, triggered), &EmbeddedSystemStatusDisplay_Value);
			ATMO_FreeValue(&EmbeddedSystemStatusDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedSystemStatusDisplay, displayPage):
		{
			ATMO_Value_t EmbeddedSystemStatusDisplay_Value;
			ATMO_InitValue(&EmbeddedSystemStatusDisplay_Value);
			EmbeddedSystemStatusDisplay_displayPage(value, &EmbeddedSystemStatusDisplay_Value);
			ATMO_FreeValue(&EmbeddedSystemStatusDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedSystemStatusDisplay, onDisplayed):
		{
			ATMO_Value_t EmbeddedSystemStatusDisplay_Value;
			ATMO_InitValue(&EmbeddedSystemStatusDisplay_Value);
			EmbeddedSystemStatusDisplay_onDisplayed(value, &EmbeddedSystemStatusDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedSystemStatusDisplay, onDisplayed), &EmbeddedSystemStatusDisplay_Value);
			ATMO_FreeValue(&EmbeddedSystemStatusDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedSystemStatusDisplay, setup):
		{
			ATMO_Value_t EmbeddedSystemStatusDisplay_Value;
			ATMO_InitValue(&EmbeddedSystemStatusDisplay_Value);
			EmbeddedSystemStatusDisplay_setup(value, &EmbeddedSystemStatusDisplay_Value);
			ATMO_FreeValue(&EmbeddedSystemStatusDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, trigger):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_trigger(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, triggered), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, setup):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_setup(value, &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, displayRootPage):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_displayRootPage(value, &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, navigateUp):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_navigateUp(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, navigateUp), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, navigateDown):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_navigateDown(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, navigateDown), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, navigateLeft):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_navigateLeft(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, navigateLeft), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, navigateRight):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_navigateRight(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, navigateRight), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, processTopRightButton):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_processTopRightButton(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, processTopRightButton), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, processBottomRightButton):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_processBottomRightButton(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, processBottomRightButton), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, processTopLeftButton):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_processTopLeftButton(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, processTopLeftButton), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedPageController, processBottomLeftButton):
		{
			ATMO_Value_t EmbeddedPageController_Value;
			ATMO_InitValue(&EmbeddedPageController_Value);
			EmbeddedPageController_processBottomLeftButton(value, &EmbeddedPageController_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedPageController, processBottomLeftButton), &EmbeddedPageController_Value);
			ATMO_FreeValue(&EmbeddedPageController_Value);
			break;
		}
		case ATMO_ABILITY(SX9500Touch, trigger):
		{
			ATMO_Value_t SX9500Touch_Value;
			ATMO_InitValue(&SX9500Touch_Value);
			SX9500Touch_trigger(value, &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, triggered), &SX9500Touch_Value);
			ATMO_FreeValue(&SX9500Touch_Value);
			break;
		}
		case ATMO_ABILITY(SX9500Touch, setup):
		{
			ATMO_Value_t SX9500Touch_Value;
			ATMO_InitValue(&SX9500Touch_Value);
			SX9500Touch_setup(value, &SX9500Touch_Value);
			ATMO_FreeValue(&SX9500Touch_Value);
			break;
		}
		case ATMO_ABILITY(SX9500Touch, getTouchData):
		{
			ATMO_Value_t SX9500Touch_Value;
			ATMO_InitValue(&SX9500Touch_Value);
			SX9500Touch_getTouchData(value, &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, touchDataRead), &SX9500Touch_Value);
			ATMO_FreeValue(&SX9500Touch_Value);
			break;
		}
		case ATMO_ABILITY(SX9500Touch, pressUp):
		{
			ATMO_Value_t SX9500Touch_Value;
			ATMO_InitValue(&SX9500Touch_Value);
			SX9500Touch_pressUp(value, &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, upPressed), &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, touchDataRead), &SX9500Touch_Value);
			ATMO_FreeValue(&SX9500Touch_Value);
			break;
		}
		case ATMO_ABILITY(SX9500Touch, pressDown):
		{
			ATMO_Value_t SX9500Touch_Value;
			ATMO_InitValue(&SX9500Touch_Value);
			SX9500Touch_pressDown(value, &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, downPressed), &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, touchDataRead), &SX9500Touch_Value);
			ATMO_FreeValue(&SX9500Touch_Value);
			break;
		}
		case ATMO_ABILITY(SX9500Touch, pressLeft):
		{
			ATMO_Value_t SX9500Touch_Value;
			ATMO_InitValue(&SX9500Touch_Value);
			SX9500Touch_pressLeft(value, &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, leftPressed), &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, touchDataRead), &SX9500Touch_Value);
			ATMO_FreeValue(&SX9500Touch_Value);
			break;
		}
		case ATMO_ABILITY(SX9500Touch, pressRight):
		{
			ATMO_Value_t SX9500Touch_Value;
			ATMO_InitValue(&SX9500Touch_Value);
			SX9500Touch_pressRight(value, &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, rightPressed), &SX9500Touch_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(SX9500Touch, touchDataRead), &SX9500Touch_Value);
			ATMO_FreeValue(&SX9500Touch_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicFireClass, trigger):
		{
			ATMO_Value_t BLECharacteristicFireClass_Value;
			ATMO_InitValue(&BLECharacteristicFireClass_Value);
			BLECharacteristicFireClass_trigger(value, &BLECharacteristicFireClass_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicFireClass, triggered), &BLECharacteristicFireClass_Value);
			ATMO_FreeValue(&BLECharacteristicFireClass_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicFireClass, setup):
		{
			ATMO_Value_t BLECharacteristicFireClass_Value;
			ATMO_InitValue(&BLECharacteristicFireClass_Value);
			BLECharacteristicFireClass_setup(value, &BLECharacteristicFireClass_Value);
			ATMO_FreeValue(&BLECharacteristicFireClass_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicFireClass, setValue):
		{
			ATMO_Value_t BLECharacteristicFireClass_Value;
			ATMO_InitValue(&BLECharacteristicFireClass_Value);
			BLECharacteristicFireClass_setValue(value, &BLECharacteristicFireClass_Value);
			ATMO_FreeValue(&BLECharacteristicFireClass_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicFireClass, written):
		{
			ATMO_Value_t BLECharacteristicFireClass_Value;
			ATMO_InitValue(&BLECharacteristicFireClass_Value);
			BLECharacteristicFireClass_written(value, &BLECharacteristicFireClass_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicFireClass, written), &BLECharacteristicFireClass_Value);
			ATMO_FreeValue(&BLECharacteristicFireClass_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicFireClass, subscibed):
		{
			ATMO_Value_t BLECharacteristicFireClass_Value;
			ATMO_InitValue(&BLECharacteristicFireClass_Value);
			BLECharacteristicFireClass_subscibed(value, &BLECharacteristicFireClass_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicFireClass, subscibed), &BLECharacteristicFireClass_Value);
			ATMO_FreeValue(&BLECharacteristicFireClass_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicFireClass, unsubscribed):
		{
			ATMO_Value_t BLECharacteristicFireClass_Value;
			ATMO_InitValue(&BLECharacteristicFireClass_Value);
			BLECharacteristicFireClass_unsubscribed(value, &BLECharacteristicFireClass_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicFireClass, unsubscribed), &BLECharacteristicFireClass_Value);
			ATMO_FreeValue(&BLECharacteristicFireClass_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, trigger):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_trigger(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedIconLinesDisplay, triggered), &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, displayPage):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_displayPage(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, onDisplayed):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_onDisplayed(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedIconLinesDisplay, onDisplayed), &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, onLeave):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_onLeave(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedIconLinesDisplay, onLeave), &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, setIconLabelAndColor):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_setIconLabelAndColor(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, setIconLabel):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_setIconLabel(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, setup):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_setup(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine1Text):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_setLine1Text(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine2Text):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_setLine2Text(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine3Text):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_setLine3Text(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine4Text):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_setLine4Text(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, topRightButtonPressed):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_topRightButtonPressed(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedIconLinesDisplay, topRightButtonPressed), &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, bottomRightButtonPressed):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_bottomRightButtonPressed(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedIconLinesDisplay, bottomRightButtonPressed), &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, topLeftButtonPressed):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_topLeftButtonPressed(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedIconLinesDisplay, topLeftButtonPressed), &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedIconLinesDisplay, bottomLeftButtonPressed):
		{
			ATMO_Value_t EmbeddedIconLinesDisplay_Value;
			ATMO_InitValue(&EmbeddedIconLinesDisplay_Value);
			EmbeddedIconLinesDisplay_bottomLeftButtonPressed(value, &EmbeddedIconLinesDisplay_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedIconLinesDisplay, bottomLeftButtonPressed), &EmbeddedIconLinesDisplay_Value);
			ATMO_FreeValue(&EmbeddedIconLinesDisplay_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkUserButtons, trigger):
		{
			ATMO_Value_t EmbeddedNxpRpkUserButtons_Value;
			ATMO_InitValue(&EmbeddedNxpRpkUserButtons_Value);
			EmbeddedNxpRpkUserButtons_trigger(value, &EmbeddedNxpRpkUserButtons_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, triggered), &EmbeddedNxpRpkUserButtons_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkUserButtons_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkUserButtons, setup):
		{
			ATMO_Value_t EmbeddedNxpRpkUserButtons_Value;
			ATMO_InitValue(&EmbeddedNxpRpkUserButtons_Value);
			EmbeddedNxpRpkUserButtons_setup(value, &EmbeddedNxpRpkUserButtons_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkUserButtons_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkUserButtons, topRightPushed):
		{
			ATMO_Value_t EmbeddedNxpRpkUserButtons_Value;
			ATMO_InitValue(&EmbeddedNxpRpkUserButtons_Value);
			EmbeddedNxpRpkUserButtons_topRightPushed(value, &EmbeddedNxpRpkUserButtons_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, topRightPushed), &EmbeddedNxpRpkUserButtons_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkUserButtons_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkUserButtons, bottomRightPushed):
		{
			ATMO_Value_t EmbeddedNxpRpkUserButtons_Value;
			ATMO_InitValue(&EmbeddedNxpRpkUserButtons_Value);
			EmbeddedNxpRpkUserButtons_bottomRightPushed(value, &EmbeddedNxpRpkUserButtons_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, bottomRightPushed), &EmbeddedNxpRpkUserButtons_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkUserButtons_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkUserButtons, topLeftPushed):
		{
			ATMO_Value_t EmbeddedNxpRpkUserButtons_Value;
			ATMO_InitValue(&EmbeddedNxpRpkUserButtons_Value);
			EmbeddedNxpRpkUserButtons_topLeftPushed(value, &EmbeddedNxpRpkUserButtons_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, topLeftPushed), &EmbeddedNxpRpkUserButtons_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkUserButtons_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkUserButtons, bottomLeftPushed):
		{
			ATMO_Value_t EmbeddedNxpRpkUserButtons_Value;
			ATMO_InitValue(&EmbeddedNxpRpkUserButtons_Value);
			EmbeddedNxpRpkUserButtons_bottomLeftPushed(value, &EmbeddedNxpRpkUserButtons_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkUserButtons, bottomLeftPushed), &EmbeddedNxpRpkUserButtons_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkUserButtons_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicAgent, trigger):
		{
			ATMO_Value_t BLECharacteristicAgent_Value;
			ATMO_InitValue(&BLECharacteristicAgent_Value);
			BLECharacteristicAgent_trigger(value, &BLECharacteristicAgent_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicAgent, triggered), &BLECharacteristicAgent_Value);
			ATMO_FreeValue(&BLECharacteristicAgent_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicAgent, setup):
		{
			ATMO_Value_t BLECharacteristicAgent_Value;
			ATMO_InitValue(&BLECharacteristicAgent_Value);
			BLECharacteristicAgent_setup(value, &BLECharacteristicAgent_Value);
			ATMO_FreeValue(&BLECharacteristicAgent_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicAgent, setValue):
		{
			ATMO_Value_t BLECharacteristicAgent_Value;
			ATMO_InitValue(&BLECharacteristicAgent_Value);
			BLECharacteristicAgent_setValue(value, &BLECharacteristicAgent_Value);
			ATMO_FreeValue(&BLECharacteristicAgent_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicAgent, written):
		{
			ATMO_Value_t BLECharacteristicAgent_Value;
			ATMO_InitValue(&BLECharacteristicAgent_Value);
			BLECharacteristicAgent_written(value, &BLECharacteristicAgent_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicAgent, written), &BLECharacteristicAgent_Value);
			ATMO_FreeValue(&BLECharacteristicAgent_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicAgent, subscibed):
		{
			ATMO_Value_t BLECharacteristicAgent_Value;
			ATMO_InitValue(&BLECharacteristicAgent_Value);
			BLECharacteristicAgent_subscibed(value, &BLECharacteristicAgent_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicAgent, subscibed), &BLECharacteristicAgent_Value);
			ATMO_FreeValue(&BLECharacteristicAgent_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicAgent, unsubscribed):
		{
			ATMO_Value_t BLECharacteristicAgent_Value;
			ATMO_InitValue(&BLECharacteristicAgent_Value);
			BLECharacteristicAgent_unsubscribed(value, &BLECharacteristicAgent_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicAgent, unsubscribed), &BLECharacteristicAgent_Value);
			ATMO_FreeValue(&BLECharacteristicAgent_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicCrew, trigger):
		{
			ATMO_Value_t BLECharacteristicCrew_Value;
			ATMO_InitValue(&BLECharacteristicCrew_Value);
			BLECharacteristicCrew_trigger(value, &BLECharacteristicCrew_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicCrew, triggered), &BLECharacteristicCrew_Value);
			ATMO_FreeValue(&BLECharacteristicCrew_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicCrew, setup):
		{
			ATMO_Value_t BLECharacteristicCrew_Value;
			ATMO_InitValue(&BLECharacteristicCrew_Value);
			BLECharacteristicCrew_setup(value, &BLECharacteristicCrew_Value);
			ATMO_FreeValue(&BLECharacteristicCrew_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicCrew, setValue):
		{
			ATMO_Value_t BLECharacteristicCrew_Value;
			ATMO_InitValue(&BLECharacteristicCrew_Value);
			BLECharacteristicCrew_setValue(value, &BLECharacteristicCrew_Value);
			ATMO_FreeValue(&BLECharacteristicCrew_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicCrew, written):
		{
			ATMO_Value_t BLECharacteristicCrew_Value;
			ATMO_InitValue(&BLECharacteristicCrew_Value);
			BLECharacteristicCrew_written(value, &BLECharacteristicCrew_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicCrew, written), &BLECharacteristicCrew_Value);
			ATMO_FreeValue(&BLECharacteristicCrew_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicCrew, subscibed):
		{
			ATMO_Value_t BLECharacteristicCrew_Value;
			ATMO_InitValue(&BLECharacteristicCrew_Value);
			BLECharacteristicCrew_subscibed(value, &BLECharacteristicCrew_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicCrew, subscibed), &BLECharacteristicCrew_Value);
			ATMO_FreeValue(&BLECharacteristicCrew_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicCrew, unsubscribed):
		{
			ATMO_Value_t BLECharacteristicCrew_Value;
			ATMO_InitValue(&BLECharacteristicCrew_Value);
			BLECharacteristicCrew_unsubscribed(value, &BLECharacteristicCrew_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicCrew, unsubscribed), &BLECharacteristicCrew_Value);
			ATMO_FreeValue(&BLECharacteristicCrew_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicColour, trigger):
		{
			ATMO_Value_t BLECharacteristicColour_Value;
			ATMO_InitValue(&BLECharacteristicColour_Value);
			BLECharacteristicColour_trigger(value, &BLECharacteristicColour_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicColour, triggered), &BLECharacteristicColour_Value);
			ATMO_FreeValue(&BLECharacteristicColour_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicColour, setup):
		{
			ATMO_Value_t BLECharacteristicColour_Value;
			ATMO_InitValue(&BLECharacteristicColour_Value);
			BLECharacteristicColour_setup(value, &BLECharacteristicColour_Value);
			ATMO_FreeValue(&BLECharacteristicColour_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicColour, setValue):
		{
			ATMO_Value_t BLECharacteristicColour_Value;
			ATMO_InitValue(&BLECharacteristicColour_Value);
			BLECharacteristicColour_setValue(value, &BLECharacteristicColour_Value);
			ATMO_FreeValue(&BLECharacteristicColour_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicColour, written):
		{
			ATMO_Value_t BLECharacteristicColour_Value;
			ATMO_InitValue(&BLECharacteristicColour_Value);
			BLECharacteristicColour_written(value, &BLECharacteristicColour_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicColour, written), &BLECharacteristicColour_Value);
			ATMO_FreeValue(&BLECharacteristicColour_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicColour, subscibed):
		{
			ATMO_Value_t BLECharacteristicColour_Value;
			ATMO_InitValue(&BLECharacteristicColour_Value);
			BLECharacteristicColour_subscibed(value, &BLECharacteristicColour_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicColour, subscibed), &BLECharacteristicColour_Value);
			ATMO_FreeValue(&BLECharacteristicColour_Value);
			break;
		}
		case ATMO_ABILITY(BLECharacteristicColour, unsubscribed):
		{
			ATMO_Value_t BLECharacteristicColour_Value;
			ATMO_InitValue(&BLECharacteristicColour_Value);
			BLECharacteristicColour_unsubscribed(value, &BLECharacteristicColour_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(BLECharacteristicColour, unsubscribed), &BLECharacteristicColour_Value);
			ATMO_FreeValue(&BLECharacteristicColour_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, trigger):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_trigger(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, triggered), &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setup):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setup(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setColorPreset):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setColorPreset(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessLow):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setBrightnessLow(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, brightnessSet), &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessOff):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setBrightnessOff(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, brightnessSet), &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessMedium):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setBrightnessMedium(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, brightnessSet), &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessHigh):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setBrightnessHigh(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, brightnessSet), &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setRedOn):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setRedOn(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setGreenOn):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setGreenOn(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBlueOn):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setBlueOn(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setWhiteOn):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setWhiteOn(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, toggleRed):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_toggleRed(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, toggleGreen):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_toggleGreen(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, toggleBlue):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_toggleBlue(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setOff):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setOff(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
		case ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setColorHex):
		{
			ATMO_Value_t EmbeddedNxpRpkRgbLed1_Value;
			ATMO_InitValue(&EmbeddedNxpRpkRgbLed1_Value);
			EmbeddedNxpRpkRgbLed1_setColorHex(value, &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_TriggerHandler(ATMO_TRIGGER(EmbeddedNxpRpkRgbLed1, colorSet), &EmbeddedNxpRpkRgbLed1_Value);
			ATMO_FreeValue(&EmbeddedNxpRpkRgbLed1_Value);
			break;
		}
	}

}

#ifdef __cplusplus
}
#endif
