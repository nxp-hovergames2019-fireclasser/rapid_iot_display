#ifndef _ATMO_CONFIG_H_
#define _ATMO_CONFIG_H_

/**
 *  Static core configuration
 *
 *  If this is defined, the Nimbus Core will not use malloc or free. Given the risks of dynamic allocation on embedded systems, this is generally a good idea.
 */
#define ATMO_STATIC_CORE
#define ATMO_STATIC_SIZE 128

// Platform uses default interval driver
// #define ATMO_DEFAULT_INTERVAL

// Platform does not have sscanf() support
// #define ATMO_PLATFORM_NO_SSCANF_SUPPORT

//  Platform does not have double support
// #define ATMO_PLATFORM_NO_DOUBLE_SUPPORT

//  Platform does not have float support
// #define ATMO_PLATFORM_NO_FLOAT_SUPPORT

/* Implement tinyprintf */
//#define ATMO_PLATFORM_TINYPRINTF

/* Platform does not have sprintf support */
// #define ATMO_PLATFORM_NO_SPRINTF_SUPPORT

/* Platform does not have sprintf float support (some platforms' sprintf implementation doesn't support floats) */
//#define ATMO_NO_SPRINTF_FLOAT_SUPPORT

/* If a platform doesn't have sprint float support, it may have dtostrf support instead */
/* NOTE: If a platform has neither float nor dtostrf support, 2 decimal fixed point will be used with integer sprintf */
//#define ATMO_NO_DTOSTRF_SUPPORT

/* No pow function available, use custom */
// #define ATMO_PLATFORM_NO_POW

/* No sqrt function available. use custom */
// #define ATMO_PLATFORM_NO_SQRT

//  Remove non-necessary prints, make buffers as small as possible to preserve the maximum amount of flash space
//  Do not enable unless absolutely necessary
//#define ATMO_SLIM_STACK

/* Platform doesn't support strtoul */
//#define ATMO_NO_STRTOUL_SUPPORT

/* Platform doesn't support strtol */
//#define ATMO_NO_STRTOL_SUPPORT

//  Size of Cloud TCP Driver Queue for Cloud Events
// #define ATMO_CLOUD_TCP_QUEUE_SIZE 10

//  Minimum baud rate a platform can support
// #define ATMO_UART_MIN_BAUD 9600

/* No boolean support. Use custom uint8_t for bool instead */
//#define ATMO_PLATFORM_NO_BOOL

/* Custom additions to atmosphere_typedefs.h. If defined, you must implement a header app_src/atmosphere_typedefs_custom.h and it will automatically be included. */
// #define ATMO_PLATFORM_CUSTOM_TYPE_INCLUDE

/* Don't include stdint.h in atmosphere_typedefs.h */
// #define ATMO_PLATFORM_NO_STDINT

/* No strcpy support. Use Atmosphere implementation */
//#define ATMO_PLATFORM_NO_STRCPY

/* No strncmp support, use Atmosphere implementation */
// #define ATMO_PLATFORM_NO_STRNCMP

/* BLE stack handles this characteristic itself */
#define ATMO_BLE_NO_SERVICE_CHANGED

// #define ATMO_PLATFORM_NO_STRTOF_SUPPORT
// #define ATMO_PLATFORM_NO_STRTOD_SUPPORT

#endif
