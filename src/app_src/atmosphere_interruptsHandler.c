#include "atmosphere_interruptsHandler.h"
#include "atmosphere_abilityHandler.h"


#ifdef __cplusplus
extern "C"{
#endif
void ATMO_BLEConnection_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, trigger), (ATMO_Value_t *)data);
}
void ATMO_BLEConnection_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, setup), (ATMO_Value_t *)data);
}
void ATMO_BLEConnection_INTERRUPT_disconnect(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, disconnect), (ATMO_Value_t *)data);
}
void ATMO_BLEConnection_INTERRUPT_connected(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, connected), (ATMO_Value_t *)data);
}
void ATMO_BLEConnection_INTERRUPT_disconnected(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, disconnected), (ATMO_Value_t *)data);
}
void ATMO_BLEConnection_INTERRUPT_pairingRequested(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, pairingRequested), (ATMO_Value_t *)data);
}
void ATMO_BLEConnection_INTERRUPT_pairingSucceeded(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, pairingSucceeded), (ATMO_Value_t *)data);
}
void ATMO_BLEConnection_INTERRUPT_pairingFailed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLEConnection, pairingFailed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedSystemStatusDisplay, trigger), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_displayPage(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedSystemStatusDisplay, displayPage), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_onDisplayed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedSystemStatusDisplay, onDisplayed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedSystemStatusDisplay, setup), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, trigger), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, setup), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_displayRootPage(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, displayRootPage), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_navigateUp(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, navigateUp), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_navigateDown(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, navigateDown), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_navigateLeft(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, navigateLeft), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_navigateRight(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, navigateRight), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_processTopRightButton(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, processTopRightButton), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_processBottomRightButton(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, processBottomRightButton), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_processTopLeftButton(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, processTopLeftButton), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedPageController_INTERRUPT_processBottomLeftButton(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedPageController, processBottomLeftButton), (ATMO_Value_t *)data);
}
void ATMO_SX9500Touch_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(SX9500Touch, trigger), (ATMO_Value_t *)data);
}
void ATMO_SX9500Touch_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(SX9500Touch, setup), (ATMO_Value_t *)data);
}
void ATMO_SX9500Touch_INTERRUPT_getTouchData(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(SX9500Touch, getTouchData), (ATMO_Value_t *)data);
}
void ATMO_SX9500Touch_INTERRUPT_pressUp(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(SX9500Touch, pressUp), (ATMO_Value_t *)data);
}
void ATMO_SX9500Touch_INTERRUPT_pressDown(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(SX9500Touch, pressDown), (ATMO_Value_t *)data);
}
void ATMO_SX9500Touch_INTERRUPT_pressLeft(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(SX9500Touch, pressLeft), (ATMO_Value_t *)data);
}
void ATMO_SX9500Touch_INTERRUPT_pressRight(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(SX9500Touch, pressRight), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicFireClass_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicFireClass, trigger), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicFireClass_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicFireClass, setup), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicFireClass_INTERRUPT_setValue(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicFireClass, setValue), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicFireClass_INTERRUPT_written(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicFireClass, written), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicFireClass_INTERRUPT_subscibed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicFireClass, subscibed), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicFireClass_INTERRUPT_unsubscribed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicFireClass, unsubscribed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, trigger), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_displayPage(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, displayPage), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_onDisplayed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, onDisplayed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_onLeave(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, onLeave), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setIconLabelAndColor(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, setIconLabelAndColor), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setIconLabel(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, setIconLabel), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, setup), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine1Text(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine1Text), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine2Text(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine2Text), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine3Text(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine3Text), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine4Text(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, setLine4Text), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_topRightButtonPressed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, topRightButtonPressed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_bottomRightButtonPressed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, bottomRightButtonPressed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_topLeftButtonPressed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, topLeftButtonPressed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_bottomLeftButtonPressed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedIconLinesDisplay, bottomLeftButtonPressed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkUserButtons, trigger), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkUserButtons, setup), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_topRightPushed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkUserButtons, topRightPushed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_bottomRightPushed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkUserButtons, bottomRightPushed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_topLeftPushed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkUserButtons, topLeftPushed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_bottomLeftPushed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkUserButtons, bottomLeftPushed), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicAgent_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicAgent, trigger), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicAgent_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicAgent, setup), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicAgent_INTERRUPT_setValue(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicAgent, setValue), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicAgent_INTERRUPT_written(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicAgent, written), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicAgent_INTERRUPT_subscibed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicAgent, subscibed), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicAgent_INTERRUPT_unsubscribed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicAgent, unsubscribed), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicCrew_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicCrew, trigger), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicCrew_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicCrew, setup), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicCrew_INTERRUPT_setValue(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicCrew, setValue), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicCrew_INTERRUPT_written(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicCrew, written), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicCrew_INTERRUPT_subscibed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicCrew, subscibed), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicCrew_INTERRUPT_unsubscribed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicCrew, unsubscribed), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicColour_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicColour, trigger), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicColour_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicColour, setup), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicColour_INTERRUPT_setValue(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicColour, setValue), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicColour_INTERRUPT_written(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicColour, written), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicColour_INTERRUPT_subscibed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicColour, subscibed), (ATMO_Value_t *)data);
}
void ATMO_BLECharacteristicColour_INTERRUPT_unsubscribed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(BLECharacteristicColour, unsubscribed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_trigger(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, trigger), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setup(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setup), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setColorPreset(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setColorPreset), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessLow(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessLow), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessOff(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessOff), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessMedium(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessMedium), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessHigh(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBrightnessHigh), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setRedOn(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setRedOn), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setGreenOn(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setGreenOn), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBlueOn(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setBlueOn), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setWhiteOn(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setWhiteOn), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_toggleRed(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, toggleRed), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_toggleGreen(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, toggleGreen), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_toggleBlue(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, toggleBlue), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setOff(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setOff), (ATMO_Value_t *)data);
}
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setColorHex(void *data) {
	ATMO_AddAbilityExecute(ATMO_ABILITY(EmbeddedNxpRpkRgbLed1, setColorHex), (ATMO_Value_t *)data);
}

#ifdef __cplusplus
}
#endif
	