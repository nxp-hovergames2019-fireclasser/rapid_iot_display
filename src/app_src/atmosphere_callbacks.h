
#ifndef ATMO_CALLBACKS_H
#define ATMO_CALLBACKS_H

#include "atmosphere_platform.h"
#include "atmosphere_properties.h"
#include "atmosphere_variables.h"
#include "atmosphere_triggerHandler.h"
#include "atmosphere_interruptsHandler.h"
#include "atmosphere_embedded_libraries.h"
#include "atmosphere_abilityHandler.h"

#include "atmosphere_driverinstance.h"

#include "atmosphere_cloudcommands.h"

#include "atmosphere_elementnames.h"

#define ATMO_CALLBACK(ELEMENT, NAME) ELEMENT ## _ ## NAME

void ATMO_Setup();

ATMO_Status_t BLEConnection_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLEConnection_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLEConnection_disconnect(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLEConnection_connected(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLEConnection_disconnected(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLEConnection_pairingRequested(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLEConnection_pairingSucceeded(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLEConnection_pairingFailed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedSystemStatusDisplay_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedSystemStatusDisplay_displayPage(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedSystemStatusDisplay_onDisplayed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedSystemStatusDisplay_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_displayRootPage(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_navigateUp(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_navigateDown(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_navigateLeft(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_navigateRight(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_processTopRightButton(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_processBottomRightButton(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_processTopLeftButton(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedPageController_processBottomLeftButton(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t SX9500Touch_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t SX9500Touch_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t SX9500Touch_getTouchData(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t SX9500Touch_pressUp(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t SX9500Touch_pressDown(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t SX9500Touch_pressLeft(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t SX9500Touch_pressRight(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicFireClass_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicFireClass_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicFireClass_setValue(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicFireClass_written(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicFireClass_subscibed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicFireClass_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_displayPage(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_onDisplayed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_onLeave(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_setIconLabelAndColor(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_setIconLabel(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_setLine1Text(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_setLine2Text(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_setLine3Text(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_setLine4Text(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_topRightButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_bottomRightButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_topLeftButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedIconLinesDisplay_bottomLeftButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkUserButtons_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkUserButtons_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkUserButtons_topRightPushed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkUserButtons_bottomRightPushed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkUserButtons_topLeftPushed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkUserButtons_bottomLeftPushed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicAgent_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicAgent_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicAgent_setValue(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicAgent_written(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicAgent_subscibed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicAgent_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicCrew_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicCrew_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicCrew_setValue(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicCrew_written(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicCrew_subscibed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicCrew_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicColour_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicColour_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicColour_setValue(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicColour_written(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicColour_subscibed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t BLECharacteristicColour_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_trigger(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setup(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setColorPreset(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessLow(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessOff(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessMedium(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessHigh(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setRedOn(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setGreenOn(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBlueOn(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setWhiteOn(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_toggleRed(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_toggleGreen(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_toggleBlue(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setOff(ATMO_Value_t *in, ATMO_Value_t *out);

ATMO_Status_t EmbeddedNxpRpkRgbLed1_setColorHex(ATMO_Value_t *in, ATMO_Value_t *out);
#endif
