
#ifndef ATMO_ELEMENT_NAMES_H
#define ATMO_ELEMENT_NAMES_H

#ifdef __cplusplus
	extern "C"{
#endif

#define ATMO_ELEMENT_NAME(ELEMENT) ATMO_ ## ELEMENT ## _NAME

#define ATMO_BLEConnection_NAME "BLEConnection"
#define ATMO_EmbeddedSystemStatusDisplay_NAME "EmbeddedSystemStatusDisplay"
#define ATMO_EmbeddedPageController_NAME "EmbeddedPageController"
#define ATMO_SX9500Touch_NAME "SX9500Touch"
#define ATMO_BLECharacteristicFireClass_NAME "BLECharacteristicFireClass"
#define ATMO_EmbeddedIconLinesDisplay_NAME "EmbeddedIconLinesDisplay"
#define ATMO_EmbeddedNxpRpkUserButtons_NAME "EmbeddedNxpRpkUserButtons"
#define ATMO_BLECharacteristicAgent_NAME "BLECharacteristicAgent"
#define ATMO_BLECharacteristicCrew_NAME "BLECharacteristicCrew"
#define ATMO_BLECharacteristicColour_NAME "BLECharacteristicColour"
#define ATMO_EmbeddedNxpRpkRgbLed1_NAME "EmbeddedNxpRpkRgbLed1"

#ifdef __cplusplus
}
#endif
#endif
