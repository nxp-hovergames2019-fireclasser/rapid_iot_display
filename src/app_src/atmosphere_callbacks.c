#include "atmosphere_callbacks.h"

//HEADER START

//HEADER END

void ATMO_Setup() {

}


ATMO_Status_t BLEConnection_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLEConnection_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_BLE_PairingCfg_t pairingCfg;
	pairingCfg.pairingKey = ATMO_PROPERTY(BLEConnection, pairingKey);
	pairingCfg.type = ATMO_PROPERTY(BLEConnection, pairingType);
	ATMO_BLE_GAPPairingCfg(ATMO_PROPERTY(BLEConnection, instance), &pairingCfg);

	ATMO_BLE_RegisterEventAbilityHandle(ATMO_PROPERTY(BLEConnection, instance), ATMO_BLE_EVENT_Connected, ATMO_ABILITY(BLEConnection, connected));
	ATMO_BLE_RegisterEventAbilityHandle(ATMO_PROPERTY(BLEConnection, instance), ATMO_BLE_EVENT_Disconnected, ATMO_ABILITY(BLEConnection, disconnected));
	ATMO_BLE_RegisterEventAbilityHandle(ATMO_PROPERTY(BLEConnection, instance), ATMO_BLE_EVENT_PairingRequested, ATMO_ABILITY(BLEConnection, pairingRequested));
	ATMO_BLE_RegisterEventAbilityHandle(ATMO_PROPERTY(BLEConnection, instance), ATMO_BLE_EVENT_PairingSuccess, ATMO_ABILITY(BLEConnection, pairingSucceeded));
	ATMO_BLE_RegisterEventAbilityHandle(ATMO_PROPERTY(BLEConnection, instance), ATMO_BLE_EVENT_PairingFailed, ATMO_ABILITY(BLEConnection, pairingFailed));
	
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLEConnection_disconnect(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_BLE_GAPDisconnect(ATMO_PROPERTY(BLEConnection, instance));
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLEConnection_connected(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t BLEConnection_disconnected(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t BLEConnection_pairingRequested(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_CreateValueCopy(out, in);
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLEConnection_pairingSucceeded(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t BLEConnection_pairingFailed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedSystemStatusDisplay_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedSystemStatusDisplay_displayPage(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_DisplayPageByCoord(ATMO_PROPERTY(EmbeddedSystemStatusDisplay, x), ATMO_PROPERTY(EmbeddedSystemStatusDisplay, y), false);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedSystemStatusDisplay_onDisplayed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedSystemStatusDisplay_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

    ATMO_UI_PAGE_Config_t config;
    config.hidden = ATMO_PROPERTY(EmbeddedSystemStatusDisplay, pageHidden);
    config.activeButtons = ATMO_UI_Page_GetButtonMask(ATMO_PROPERTY(EmbeddedSystemStatusDisplay, topRightButtonEnabled),
    ATMO_PROPERTY(EmbeddedSystemStatusDisplay,bottomRightButtonEnabled), ATMO_PROPERTY(EmbeddedSystemStatusDisplay, topLeftButtonEnabled), ATMO_PROPERTY(EmbeddedSystemStatusDisplay, bottomLeftButtonEnabled));
    config.x = ATMO_PROPERTY(EmbeddedSystemStatusDisplay, x);
    config.y = ATMO_PROPERTY(EmbeddedSystemStatusDisplay, y);
	strncpy(config.topLeftButtonLabel, ATMO_PROPERTY(EmbeddedSystemStatusDisplay, topLeftButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
	strncpy(config.topRightButtonLabel, ATMO_PROPERTY(EmbeddedSystemStatusDisplay, topRightButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
	strncpy(config.bottomLeftButtonLabel, ATMO_PROPERTY(EmbeddedSystemStatusDisplay, bottomLeftButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
	strncpy(config.bottomRightButtonLabel, ATMO_PROPERTY(EmbeddedSystemStatusDisplay, bottomRightButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
    config.spanX = ATMO_PROPERTY(EmbeddedSystemStatusDisplay, spanX);
	config.spanY = ATMO_PROPERTY(EmbeddedSystemStatusDisplay, spanY);
    config.title = ATMO_PROPERTY(EmbeddedSystemStatusDisplay, pageTitle);
    config.titleHidden = ATMO_PROPERTY(EmbeddedSystemStatusDisplay, titleHidden);
    ATMO_UI_SYSTEMSTATUS_Init(&config);
    
}


ATMO_Status_t EmbeddedPageController_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedPageController_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_PAGE_CONTROLLER_Config_t config;
	config.enableUpDownNavLabels = ATMO_PROPERTY(EmbeddedPageController, upDownNavigationLabelsEnabled);
	config.enableLeftRightNavLabels = ATMO_PROPERTY(EmbeddedPageController, leftRightNavigationLabelsEnabled);
	ATMO_UI_Page_SetConfiguration(&config);
    return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_displayRootPage(ATMO_Value_t *in, ATMO_Value_t *out) {

	
	ATMO_UI_Page_DisplayRootPage();
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_navigateUp(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessNavButton(ATMO_UI_PAGE_NAV_UP);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_navigateDown(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessNavButton(ATMO_UI_PAGE_NAV_DOWN);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_navigateLeft(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessNavButton(ATMO_UI_PAGE_NAV_LEFT);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_navigateRight(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessNavButton(ATMO_UI_PAGE_NAV_RIGHT);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_processTopRightButton(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessUserButton(1);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_processBottomRightButton(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessUserButton(2);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_processTopLeftButton(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessUserButton(3);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedPageController_processBottomLeftButton(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_ProcessUserButton(4);
	return ATMO_Status_Success;
	
}


ATMO_Status_t SX9500Touch_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t SX9500Touch_setup(ATMO_Value_t *in, ATMO_Value_t *out) {
	ATMO_SX9500_Config_t config;
	config.address = ATMO_PROPERTY(SX9500Touch, i2cAddress);
	config.i2cDriverInstance = ATMO_PROPERTY(SX9500Touch, i2cInstance);
	config.gpioDriverInstance = ATMO_PROPERTY(SX9500Touch, gpioInstance);
	config.interruptEnabled = ATMO_PROPERTY(SX9500Touch, interruptEnabled);
	config.interruptPin = ATMO_PROPERTY(SX9500Touch, interruptGpio);
	ATMO_SX9500_Init(&config);
	ATMO_SX9500_RegisterTouchedAbilityHandle(SX9500_Touched_Up, ATMO_ABILITY(SX9500Touch, pressUp));
	ATMO_SX9500_RegisterTouchedAbilityHandle(SX9500_Touched_Down, ATMO_ABILITY(SX9500Touch, pressDown));
	ATMO_SX9500_RegisterTouchedAbilityHandle(SX9500_Touched_Left, ATMO_ABILITY(SX9500Touch, pressLeft));
	ATMO_SX9500_RegisterTouchedAbilityHandle(SX9500_Touched_Right, ATMO_ABILITY(SX9500Touch, pressRight));
	return ATMO_Status_Success;
}


ATMO_Status_t SX9500Touch_getTouchData(ATMO_Value_t *in, ATMO_Value_t *out) {
	return;
}


ATMO_Status_t SX9500Touch_pressUp(ATMO_Value_t *in, ATMO_Value_t *out) {
SX9500_TouchState_t touchState;
ATMO_GetBinary(in, &touchState, sizeof(touchState));
ATMO_CreateValueBinary(out, &touchState, sizeof(touchState));
return ATMO_Status_Success;
}


ATMO_Status_t SX9500Touch_pressDown(ATMO_Value_t *in, ATMO_Value_t *out) {
SX9500_TouchState_t touchState;
ATMO_GetBinary(in, &touchState, sizeof(touchState));
ATMO_CreateValueBinary(out, &touchState, sizeof(touchState));
return ATMO_Status_Success;
}


ATMO_Status_t SX9500Touch_pressLeft(ATMO_Value_t *in, ATMO_Value_t *out) {
SX9500_TouchState_t touchState;
ATMO_GetBinary(in, &touchState, sizeof(touchState));
ATMO_CreateValueBinary(out, &touchState, sizeof(touchState));
return ATMO_Status_Success;
}


ATMO_Status_t SX9500Touch_pressRight(ATMO_Value_t *in, ATMO_Value_t *out) {
SX9500_TouchState_t touchState;
ATMO_GetBinary(in, &touchState, sizeof(touchState));
ATMO_CreateValueBinary(out, &touchState, sizeof(touchState));
return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicFireClass_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicFireClass_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_BLE_GATTSAddService(
		ATMO_PROPERTY(BLECharacteristicFireClass, instance),
		&ATMO_VARIABLE(BLECharacteristicFireClass, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicFireClass, bleServiceUuid));
	
	uint8_t property = 0;
	uint8_t permission = 0;
	
	property |= ATMO_PROPERTY(BLECharacteristicFireClass, read) ? ATMO_BLE_Property_Read : 0;
	property |= ATMO_PROPERTY(BLECharacteristicFireClass, write) ? ATMO_BLE_Property_Write : 0;
	property |= ATMO_PROPERTY(BLECharacteristicFireClass, notify) ? ATMO_BLE_Property_Notify : 0;

	permission |= ATMO_PROPERTY(BLECharacteristicFireClass, read) ? ATMO_BLE_Permission_Read : 0;
	permission |= ATMO_PROPERTY(BLECharacteristicFireClass, write) ? ATMO_BLE_Permission_Write : 0;

	ATMO_DATATYPE types[3] = {ATMO_PROPERTY(BLECharacteristicFireClass, writeDataType), ATMO_PROPERTY(BLECharacteristicFireClass, readDataType), ATMO_PROPERTY(BLECharacteristicFireClass, notifyDataType)};
	
	ATMO_BLE_GATTSAddCharacteristic(
		ATMO_PROPERTY(BLECharacteristicFireClass, instance),
		&ATMO_VARIABLE(BLECharacteristicFireClass, bleCharacteristicHandle), 
		ATMO_VARIABLE(BLECharacteristicFireClass, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicFireClass, bleCharacteristicUuid), 
		property, permission, ATMO_GetMaxValueSize(3, 64, types));
	
	ATMO_BLE_GATTSRegisterCharacteristicAbilityHandle(
		ATMO_PROPERTY(BLECharacteristicFireClass, instance),
		ATMO_VARIABLE(BLECharacteristicFireClass, bleCharacteristicHandle), 
		ATMO_BLE_Characteristic_Written, 
		ATMO_ABILITY(BLECharacteristicFireClass, written));
	
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicFireClass_setValue(ATMO_Value_t *in, ATMO_Value_t *out) {

	
	// Convert to the desired write data type
	ATMO_Value_t convertedValue;
	ATMO_InitValue(&convertedValue);
	ATMO_CreateValueConverted(&convertedValue, ATMO_PROPERTY(BLECharacteristicFireClass, readDataType), in);

	ATMO_BLE_GATTSSetCharacteristic(
		ATMO_PROPERTY(BLECharacteristicFireClass, instance),
		ATMO_VARIABLE(BLECharacteristicFireClass, bleCharacteristicHandle),
		convertedValue.size, 
		(uint8_t *)convertedValue.data,
		NULL);
	
	ATMO_FreeValue(&convertedValue);
		
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicFireClass_written(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_CreateValueConverted(out, ATMO_PROPERTY(BLECharacteristicFireClass, writeDataType), in);
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicFireClass_subscibed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicFireClass_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedIconLinesDisplay_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedIconLinesDisplay_displayPage(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_UI_Page_DisplayPageByCoord(ATMO_PROPERTY(EmbeddedIconLinesDisplay, x), ATMO_PROPERTY(EmbeddedIconLinesDisplay, y), false);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedIconLinesDisplay_onDisplayed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_onLeave(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedIconLinesDisplay_setIconLabelAndColor(ATMO_Value_t *in, ATMO_Value_t *out) {

    struct {
        char str[32];
        GUI_COLOR color;
    } icon_data;
 
    ATMO_GetBinary(in, &icon_data, sizeof(icon_data));
    ATMO_UI_ICONLINES_SetIconLabelColor(ATMO_VARIABLE(EmbeddedIconLinesDisplay, pageHandle), icon_data.str, icon_data.color);
    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_setIconLabel(ATMO_Value_t *in, ATMO_Value_t *out) {

    char str[32];
    ATMO_GetString(in, str, 32);
    ATMO_UI_ICONLINES_SetIconLabel(ATMO_VARIABLE(EmbeddedIconLinesDisplay, pageHandle), str);
    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

    ATMO_UI_PAGE_Config_t config;
    config.hidden = ATMO_PROPERTY(EmbeddedIconLinesDisplay, pageHidden);
    config.textColor = ATMO_PROPERTY(EmbeddedIconLinesDisplay, textColor);
    config.activeButtons = ATMO_UI_Page_GetButtonMask(ATMO_PROPERTY(EmbeddedIconLinesDisplay, topRightButtonEnabled),
    ATMO_PROPERTY(EmbeddedIconLinesDisplay,bottomRightButtonEnabled), ATMO_PROPERTY(EmbeddedIconLinesDisplay, topLeftButtonEnabled), ATMO_PROPERTY(EmbeddedIconLinesDisplay, bottomLeftButtonEnabled));
	config.x = ATMO_PROPERTY(EmbeddedIconLinesDisplay, x);
    config.y = ATMO_PROPERTY(EmbeddedIconLinesDisplay, y);
	strncpy(config.topLeftButtonLabel, ATMO_PROPERTY(EmbeddedIconLinesDisplay, topLeftButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
	strncpy(config.topRightButtonLabel, ATMO_PROPERTY(EmbeddedIconLinesDisplay, topRightButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
	strncpy(config.bottomLeftButtonLabel, ATMO_PROPERTY(EmbeddedIconLinesDisplay, bottomLeftButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
	strncpy(config.bottomRightButtonLabel, ATMO_PROPERTY(EmbeddedIconLinesDisplay, bottomRightButtonLabel), ATMO_BUTTON_LABEL_MAXLEN);
    config.spanX = ATMO_PROPERTY(EmbeddedIconLinesDisplay, spanX);
	config.spanY = ATMO_PROPERTY(EmbeddedIconLinesDisplay, spanY);
    config.title = ATMO_PROPERTY(EmbeddedIconLinesDisplay, pageTitle);
    config.titleHidden = ATMO_PROPERTY(EmbeddedIconLinesDisplay, titleHidden);
	ATMO_UI_ICONLINES_Init(&config, ATMO_PROPERTY(EmbeddedIconLinesDisplay, numLines), false);
	ATMO_VARIABLE(EmbeddedIconLinesDisplay, pageHandle) = config.templateInstance;
    ATMO_UI_ICONLINES_SetMainText(config.templateInstance, 0, ATMO_PROPERTY(EmbeddedIconLinesDisplay, line1Text));
    ATMO_UI_ICONLINES_SetMainText(config.templateInstance, 1, ATMO_PROPERTY(EmbeddedIconLinesDisplay, line2Text));
    ATMO_UI_ICONLINES_SetMainText(config.templateInstance, 2, ATMO_PROPERTY(EmbeddedIconLinesDisplay, line3Text));
    ATMO_UI_ICONLINES_SetMainText(config.templateInstance, 3, ATMO_PROPERTY(EmbeddedIconLinesDisplay, line4Text));
    ATMO_UI_ICONLINES_SetIconLabel(config.templateInstance, ATMO_PROPERTY(EmbeddedIconLinesDisplay, iconLabel));
    ATMO_UI_ICONLINES_RegisterButtonAbilityHandle(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 1, ATMO_ABILITY(EmbeddedIconLinesDisplay, topRightButtonPressed));
	ATMO_UI_ICONLINES_RegisterButtonAbilityHandle(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 2, ATMO_ABILITY(EmbeddedIconLinesDisplay, bottomRightButtonPressed));
	ATMO_UI_ICONLINES_RegisterButtonAbilityHandle(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 3, ATMO_ABILITY(EmbeddedIconLinesDisplay, topLeftButtonPressed));
    ATMO_UI_ICONLINES_RegisterButtonAbilityHandle(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 4, ATMO_ABILITY(EmbeddedIconLinesDisplay, bottomLeftButtonPressed));
    ATMO_UI_ICONLINES_SetIcon(config.templateInstance, ATMO_PROPERTY(EmbeddedIconLinesDisplay, icon));
    ATMO_UI_ICONLINES_RegisterOnDisplayedAbilityHandle(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), ATMO_ABILITY(EmbeddedIconLinesDisplay, onDisplayed));
    ATMO_UI_ICONLINES_RegisterOnLeaveAbilityHandle(config.templateInstance, ATMO_ABILITY(EmbeddedIconLinesDisplay, onLeave));
    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_setLine1Text(ATMO_Value_t *in, ATMO_Value_t *out) {

    char label[32];
    if(ATMO_GetString(in, label, 32) == ATMO_Status_Success)
    {
        ATMO_UI_ICONLINES_SetMainText(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 0, label);
    }
    else
    {
        return ATMO_Status_Fail;
    }

    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_setLine2Text(ATMO_Value_t *in, ATMO_Value_t *out) {

    char label[32];
    if(ATMO_GetString(in, label, 32) == ATMO_Status_Success)
    {
        ATMO_UI_ICONLINES_SetMainText(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 1, label);
    }
    else
    {
        return ATMO_Status_Fail;
    }

    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_setLine3Text(ATMO_Value_t *in, ATMO_Value_t *out) {

    char label[32];
    if(ATMO_GetString(in, label, 32) == ATMO_Status_Success)
    {
        ATMO_UI_ICONLINES_SetMainText(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 2, label);
    }
    else
    {
        return ATMO_Status_Fail;
    }

    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_setLine4Text(ATMO_Value_t *in, ATMO_Value_t *out) {

    char label[32];
    if(ATMO_GetString(in, label, 32) == ATMO_Status_Success)
    {
        ATMO_UI_ICONLINES_SetMainText(ATMO_VARIABLE(EmbeddedIconLinesDisplay,pageHandle), 3, label);
    }
    else
    {
        return ATMO_Status_Fail;
    }

    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedIconLinesDisplay_topRightButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedIconLinesDisplay_bottomRightButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedIconLinesDisplay_topLeftButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedIconLinesDisplay_bottomLeftButtonPressed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedNxpRpkUserButtons_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedNxpRpkUserButtons_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

    ATMO_MK64F_GPIO_EnableResetCombo(ATMO_PROPERTY(EmbeddedNxpRpkUserButtons, enableResetCombo));
    ATMO_MK64F_GPIO_EnableDisplayToggleCombo(ATMO_PROPERTY(EmbeddedNxpRpkUserButtons, enableDisplayToggleCombo));
    ATMO_MK64F_UserButton_RegisterAbilityHandle(ATMO_MK64F_SW1_Pushed, ATMO_ABILITY(EmbeddedNxpRpkUserButtons, topRightPushed));
    ATMO_MK64F_UserButton_RegisterAbilityHandle(ATMO_MK64F_SW2_Pushed, ATMO_ABILITY(EmbeddedNxpRpkUserButtons, bottomRightPushed));
    ATMO_MK64F_UserButton_RegisterAbilityHandle(ATMO_MK64F_SW3_Pushed, ATMO_ABILITY(EmbeddedNxpRpkUserButtons, topLeftPushed));
    ATMO_MK64F_UserButton_RegisterAbilityHandle(ATMO_MK64F_SW4_Pushed, ATMO_ABILITY(EmbeddedNxpRpkUserButtons, bottomLeftPushed));
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedNxpRpkUserButtons_topRightPushed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkUserButtons_bottomRightPushed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkUserButtons_topLeftPushed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkUserButtons_bottomLeftPushed(ATMO_Value_t *in, ATMO_Value_t *out) {

	return ATMO_Status_Success;
    
}


ATMO_Status_t BLECharacteristicAgent_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicAgent_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_BLE_GATTSAddService(
		ATMO_PROPERTY(BLECharacteristicAgent, instance),
		&ATMO_VARIABLE(BLECharacteristicAgent, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicAgent, bleServiceUuid));
	
	uint8_t property = 0;
	uint8_t permission = 0;
	
	property |= ATMO_PROPERTY(BLECharacteristicAgent, read) ? ATMO_BLE_Property_Read : 0;
	property |= ATMO_PROPERTY(BLECharacteristicAgent, write) ? ATMO_BLE_Property_Write : 0;
	property |= ATMO_PROPERTY(BLECharacteristicAgent, notify) ? ATMO_BLE_Property_Notify : 0;

	permission |= ATMO_PROPERTY(BLECharacteristicAgent, read) ? ATMO_BLE_Permission_Read : 0;
	permission |= ATMO_PROPERTY(BLECharacteristicAgent, write) ? ATMO_BLE_Permission_Write : 0;

	ATMO_DATATYPE types[3] = {ATMO_PROPERTY(BLECharacteristicAgent, writeDataType), ATMO_PROPERTY(BLECharacteristicAgent, readDataType), ATMO_PROPERTY(BLECharacteristicAgent, notifyDataType)};
	
	ATMO_BLE_GATTSAddCharacteristic(
		ATMO_PROPERTY(BLECharacteristicAgent, instance),
		&ATMO_VARIABLE(BLECharacteristicAgent, bleCharacteristicHandle), 
		ATMO_VARIABLE(BLECharacteristicAgent, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicAgent, bleCharacteristicUuid), 
		property, permission, ATMO_GetMaxValueSize(3, 64, types));
	
	ATMO_BLE_GATTSRegisterCharacteristicAbilityHandle(
		ATMO_PROPERTY(BLECharacteristicAgent, instance),
		ATMO_VARIABLE(BLECharacteristicAgent, bleCharacteristicHandle), 
		ATMO_BLE_Characteristic_Written, 
		ATMO_ABILITY(BLECharacteristicAgent, written));
	
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicAgent_setValue(ATMO_Value_t *in, ATMO_Value_t *out) {

	
	// Convert to the desired write data type
	ATMO_Value_t convertedValue;
	ATMO_InitValue(&convertedValue);
	ATMO_CreateValueConverted(&convertedValue, ATMO_PROPERTY(BLECharacteristicAgent, readDataType), in);

	ATMO_BLE_GATTSSetCharacteristic(
		ATMO_PROPERTY(BLECharacteristicAgent, instance),
		ATMO_VARIABLE(BLECharacteristicAgent, bleCharacteristicHandle),
		convertedValue.size, 
		(uint8_t *)convertedValue.data,
		NULL);
	
	ATMO_FreeValue(&convertedValue);
		
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicAgent_written(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_CreateValueConverted(out, ATMO_PROPERTY(BLECharacteristicAgent, writeDataType), in);
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicAgent_subscibed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicAgent_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicCrew_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicCrew_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_BLE_GATTSAddService(
		ATMO_PROPERTY(BLECharacteristicCrew, instance),
		&ATMO_VARIABLE(BLECharacteristicCrew, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicCrew, bleServiceUuid));
	
	uint8_t property = 0;
	uint8_t permission = 0;
	
	property |= ATMO_PROPERTY(BLECharacteristicCrew, read) ? ATMO_BLE_Property_Read : 0;
	property |= ATMO_PROPERTY(BLECharacteristicCrew, write) ? ATMO_BLE_Property_Write : 0;
	property |= ATMO_PROPERTY(BLECharacteristicCrew, notify) ? ATMO_BLE_Property_Notify : 0;

	permission |= ATMO_PROPERTY(BLECharacteristicCrew, read) ? ATMO_BLE_Permission_Read : 0;
	permission |= ATMO_PROPERTY(BLECharacteristicCrew, write) ? ATMO_BLE_Permission_Write : 0;

	ATMO_DATATYPE types[3] = {ATMO_PROPERTY(BLECharacteristicCrew, writeDataType), ATMO_PROPERTY(BLECharacteristicCrew, readDataType), ATMO_PROPERTY(BLECharacteristicCrew, notifyDataType)};
	
	ATMO_BLE_GATTSAddCharacteristic(
		ATMO_PROPERTY(BLECharacteristicCrew, instance),
		&ATMO_VARIABLE(BLECharacteristicCrew, bleCharacteristicHandle), 
		ATMO_VARIABLE(BLECharacteristicCrew, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicCrew, bleCharacteristicUuid), 
		property, permission, ATMO_GetMaxValueSize(3, 64, types));
	
	ATMO_BLE_GATTSRegisterCharacteristicAbilityHandle(
		ATMO_PROPERTY(BLECharacteristicCrew, instance),
		ATMO_VARIABLE(BLECharacteristicCrew, bleCharacteristicHandle), 
		ATMO_BLE_Characteristic_Written, 
		ATMO_ABILITY(BLECharacteristicCrew, written));
	
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicCrew_setValue(ATMO_Value_t *in, ATMO_Value_t *out) {

	
	// Convert to the desired write data type
	ATMO_Value_t convertedValue;
	ATMO_InitValue(&convertedValue);
	ATMO_CreateValueConverted(&convertedValue, ATMO_PROPERTY(BLECharacteristicCrew, readDataType), in);

	ATMO_BLE_GATTSSetCharacteristic(
		ATMO_PROPERTY(BLECharacteristicCrew, instance),
		ATMO_VARIABLE(BLECharacteristicCrew, bleCharacteristicHandle),
		convertedValue.size, 
		(uint8_t *)convertedValue.data,
		NULL);
	
	ATMO_FreeValue(&convertedValue);
		
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicCrew_written(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_CreateValueConverted(out, ATMO_PROPERTY(BLECharacteristicCrew, writeDataType), in);
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicCrew_subscibed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicCrew_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicColour_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicColour_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_BLE_GATTSAddService(
		ATMO_PROPERTY(BLECharacteristicColour, instance),
		&ATMO_VARIABLE(BLECharacteristicColour, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicColour, bleServiceUuid));
	
	uint8_t property = 0;
	uint8_t permission = 0;
	
	property |= ATMO_PROPERTY(BLECharacteristicColour, read) ? ATMO_BLE_Property_Read : 0;
	property |= ATMO_PROPERTY(BLECharacteristicColour, write) ? ATMO_BLE_Property_Write : 0;
	property |= ATMO_PROPERTY(BLECharacteristicColour, notify) ? ATMO_BLE_Property_Notify : 0;

	permission |= ATMO_PROPERTY(BLECharacteristicColour, read) ? ATMO_BLE_Permission_Read : 0;
	permission |= ATMO_PROPERTY(BLECharacteristicColour, write) ? ATMO_BLE_Permission_Write : 0;

	ATMO_DATATYPE types[3] = {ATMO_PROPERTY(BLECharacteristicColour, writeDataType), ATMO_PROPERTY(BLECharacteristicColour, readDataType), ATMO_PROPERTY(BLECharacteristicColour, notifyDataType)};
	
	ATMO_BLE_GATTSAddCharacteristic(
		ATMO_PROPERTY(BLECharacteristicColour, instance),
		&ATMO_VARIABLE(BLECharacteristicColour, bleCharacteristicHandle), 
		ATMO_VARIABLE(BLECharacteristicColour, bleServiceHandle), 
		ATMO_PROPERTY(BLECharacteristicColour, bleCharacteristicUuid), 
		property, permission, ATMO_GetMaxValueSize(3, 64, types));
	
	ATMO_BLE_GATTSRegisterCharacteristicAbilityHandle(
		ATMO_PROPERTY(BLECharacteristicColour, instance),
		ATMO_VARIABLE(BLECharacteristicColour, bleCharacteristicHandle), 
		ATMO_BLE_Characteristic_Written, 
		ATMO_ABILITY(BLECharacteristicColour, written));
	
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicColour_setValue(ATMO_Value_t *in, ATMO_Value_t *out) {

	
	// Convert to the desired write data type
	ATMO_Value_t convertedValue;
	ATMO_InitValue(&convertedValue);
	ATMO_CreateValueConverted(&convertedValue, ATMO_PROPERTY(BLECharacteristicColour, readDataType), in);

	ATMO_BLE_GATTSSetCharacteristic(
		ATMO_PROPERTY(BLECharacteristicColour, instance),
		ATMO_VARIABLE(BLECharacteristicColour, bleCharacteristicHandle),
		convertedValue.size, 
		(uint8_t *)convertedValue.data,
		NULL);
	
	ATMO_FreeValue(&convertedValue);
		
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicColour_written(ATMO_Value_t *in, ATMO_Value_t *out) {

	ATMO_CreateValueConverted(out, ATMO_PROPERTY(BLECharacteristicColour, writeDataType), in);
	return ATMO_Status_Success;
	
}


ATMO_Status_t BLECharacteristicColour_subscibed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t BLECharacteristicColour_unsubscribed(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_trigger(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setup(ATMO_Value_t *in, ATMO_Value_t *out) {

    RGB_Led_Set_State(RGB_LED_BRIGHT_HIGH, RGB_LED_COLOR_BLACK);
	return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setColorPreset(ATMO_Value_t *in, ATMO_Value_t *out) {

    unsigned int desiredColor = 0;
    ATMO_GetUnsignedInt(in, &desiredColor);

    uint8_t brightness = 0;
    uint8_t color = 0;
    RGB_Led_Get_State(&brightness, &color);
    RGB_Led_Set_State(brightness, desiredColor);
    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessLow(ATMO_Value_t *in, ATMO_Value_t *out) {

    uint8_t brightness = 0;
    uint8_t color = 0;
    RGB_Led_Get_State(&brightness, &color);
    RGB_Led_Set_State(RGB_LED_BRIGHT_LOW, color);
    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessOff(ATMO_Value_t *in, ATMO_Value_t *out) {

    uint8_t brightness = 0;
    uint8_t color = 0;
    RGB_Led_Get_State(&brightness, &color);
    RGB_Led_Set_State(RGB_LED_BRIGHT_OFF, color);
    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessMedium(ATMO_Value_t *in, ATMO_Value_t *out) {

    uint8_t brightness = 0;
    uint8_t color = 0;
    RGB_Led_Get_State(&brightness, &color);
    RGB_Led_Set_State(RGB_LED_BRIGHT_MEDIUM, color);
    return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBrightnessHigh(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setRedOn(ATMO_Value_t *in, ATMO_Value_t *out) {

    RGB_Led_Set_State(RGB_LED_BRIGHT_HIGH, RGB_LED_COLOR_RED);
	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setGreenOn(ATMO_Value_t *in, ATMO_Value_t *out) {

    RGB_Led_Set_State(RGB_LED_BRIGHT_HIGH, RGB_LED_COLOR_GREEN);
	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setBlueOn(ATMO_Value_t *in, ATMO_Value_t *out) {

    RGB_Led_Set_State(RGB_LED_BRIGHT_HIGH, RGB_LED_COLOR_BLUE);
	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setWhiteOn(ATMO_Value_t *in, ATMO_Value_t *out) {
	return ATMO_Status_Success;
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_toggleRed(ATMO_Value_t *in, ATMO_Value_t *out) {

	uint8_t color, brightness;
    RGB_Led_Get_State(&brightness, &color);
    if(brightness == RGB_LED_BRIGHT_OFF || color == RGB_LED_COLOR_BLACK)
    {
        brightness = RGB_LED_BRIGHT_HIGH;
    }
    else
    {
        brightness = RGB_LED_BRIGHT_OFF;
    }

    RGB_Led_Set_State(brightness, RGB_LED_COLOR_RED);
    return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_toggleGreen(ATMO_Value_t *in, ATMO_Value_t *out) {

	uint8_t color, brightness;
    RGB_Led_Get_State(&brightness, &color);
    if(brightness == RGB_LED_BRIGHT_OFF || color == RGB_LED_COLOR_BLACK)
    {
        brightness = RGB_LED_BRIGHT_HIGH;
    }
    else
    {
        brightness = RGB_LED_BRIGHT_OFF;
    }

    RGB_Led_Set_State(brightness, RGB_LED_COLOR_GREEN);
    return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_toggleBlue(ATMO_Value_t *in, ATMO_Value_t *out) {

	uint8_t color, brightness;
    RGB_Led_Get_State(&brightness, &color);
    if(brightness == RGB_LED_BRIGHT_OFF || color == RGB_LED_COLOR_BLACK)
    {
        brightness = RGB_LED_BRIGHT_HIGH;
    }
    else
    {
        brightness = RGB_LED_BRIGHT_OFF;
    }

    RGB_Led_Set_State(brightness, RGB_LED_COLOR_BLUE);
    return ATMO_Status_Success;
	
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setOff(ATMO_Value_t *in, ATMO_Value_t *out) {

    uint8_t color, brightness;
    RGB_Led_Get_State(&brightness, &color);
    RGB_Led_Set_State(brightness, RGB_LED_COLOR_BLACK);
	return ATMO_Status_Success;
    
}


ATMO_Status_t EmbeddedNxpRpkRgbLed1_setColorHex(ATMO_Value_t *in, ATMO_Value_t *out) {

    unsigned int color = 0;
    ATMO_GetUnsignedInt(in, &color);
    uint8_t red = (color >> 16) & 0xFF;
    uint8_t green = (color >> 8) & 0xFF;
    uint8_t blue = color & 0xFF;
    Rgb_Led_Set_Colors(red, green, blue);  
    ATMO_CreateValueUnsignedInt(out, color);
	return ATMO_Status_Success;
    
}

//FOOTER START

//FOOTER END

