#include "atmosphere_elementSetup.h"

#ifdef __cplusplus
	extern "C"{
#endif

void ATMO_ElementSetup() {
	ATMO_Value_t inOutValue;
	ATMO_InitValue(&inOutValue);
	BLEConnection_setup(&inOutValue, &inOutValue);
	EmbeddedSystemStatusDisplay_setup(&inOutValue, &inOutValue);
	EmbeddedPageController_setup(&inOutValue, &inOutValue);
	SX9500Touch_setup(&inOutValue, &inOutValue);
	BLECharacteristicFireClass_setup(&inOutValue, &inOutValue);
	EmbeddedIconLinesDisplay_setup(&inOutValue, &inOutValue);
	EmbeddedNxpRpkUserButtons_setup(&inOutValue, &inOutValue);
	BLECharacteristicAgent_setup(&inOutValue, &inOutValue);
	BLECharacteristicCrew_setup(&inOutValue, &inOutValue);
	BLECharacteristicColour_setup(&inOutValue, &inOutValue);
	EmbeddedNxpRpkRgbLed1_setup(&inOutValue, &inOutValue);
}

#ifdef __cplusplus
}
#endif
