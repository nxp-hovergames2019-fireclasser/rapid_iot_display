
#ifndef ATMO_INTERRUPTS_HANDLER_H
#define ATMO_INTERRUPTS_HANDLER_H

#ifdef __cplusplus
	extern "C"{
#endif

#include "../atmo/core.h"
#define ATMO_INTERRUPT(ELEMENT, NAME) ATMO_ ## ELEMENT ## _INTERRUPT_  ## NAME

void ATMO_BLEConnection_INTERRUPT_trigger(void *data);
void ATMO_BLEConnection_INTERRUPT_setup(void *data);
void ATMO_BLEConnection_INTERRUPT_disconnect(void *data);
void ATMO_BLEConnection_INTERRUPT_connected(void *data);
void ATMO_BLEConnection_INTERRUPT_disconnected(void *data);
void ATMO_BLEConnection_INTERRUPT_pairingRequested(void *data);
void ATMO_BLEConnection_INTERRUPT_pairingSucceeded(void *data);
void ATMO_BLEConnection_INTERRUPT_pairingFailed(void *data);

void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_trigger(void *data);
void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_displayPage(void *data);
void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_onDisplayed(void *data);
void ATMO_EmbeddedSystemStatusDisplay_INTERRUPT_setup(void *data);

void ATMO_EmbeddedPageController_INTERRUPT_trigger(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_setup(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_displayRootPage(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_navigateUp(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_navigateDown(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_navigateLeft(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_navigateRight(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_processTopRightButton(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_processBottomRightButton(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_processTopLeftButton(void *data);
void ATMO_EmbeddedPageController_INTERRUPT_processBottomLeftButton(void *data);

void ATMO_SX9500Touch_INTERRUPT_trigger(void *data);
void ATMO_SX9500Touch_INTERRUPT_setup(void *data);
void ATMO_SX9500Touch_INTERRUPT_getTouchData(void *data);
void ATMO_SX9500Touch_INTERRUPT_pressUp(void *data);
void ATMO_SX9500Touch_INTERRUPT_pressDown(void *data);
void ATMO_SX9500Touch_INTERRUPT_pressLeft(void *data);
void ATMO_SX9500Touch_INTERRUPT_pressRight(void *data);

void ATMO_BLECharacteristicFireClass_INTERRUPT_trigger(void *data);
void ATMO_BLECharacteristicFireClass_INTERRUPT_setup(void *data);
void ATMO_BLECharacteristicFireClass_INTERRUPT_setValue(void *data);
void ATMO_BLECharacteristicFireClass_INTERRUPT_written(void *data);
void ATMO_BLECharacteristicFireClass_INTERRUPT_subscibed(void *data);
void ATMO_BLECharacteristicFireClass_INTERRUPT_unsubscribed(void *data);

void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_trigger(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_displayPage(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_onDisplayed(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_onLeave(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setIconLabelAndColor(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setIconLabel(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setup(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine1Text(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine2Text(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine3Text(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_setLine4Text(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_topRightButtonPressed(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_bottomRightButtonPressed(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_topLeftButtonPressed(void *data);
void ATMO_EmbeddedIconLinesDisplay_INTERRUPT_bottomLeftButtonPressed(void *data);

void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_trigger(void *data);
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_setup(void *data);
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_topRightPushed(void *data);
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_bottomRightPushed(void *data);
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_topLeftPushed(void *data);
void ATMO_EmbeddedNxpRpkUserButtons_INTERRUPT_bottomLeftPushed(void *data);

void ATMO_BLECharacteristicAgent_INTERRUPT_trigger(void *data);
void ATMO_BLECharacteristicAgent_INTERRUPT_setup(void *data);
void ATMO_BLECharacteristicAgent_INTERRUPT_setValue(void *data);
void ATMO_BLECharacteristicAgent_INTERRUPT_written(void *data);
void ATMO_BLECharacteristicAgent_INTERRUPT_subscibed(void *data);
void ATMO_BLECharacteristicAgent_INTERRUPT_unsubscribed(void *data);

void ATMO_BLECharacteristicCrew_INTERRUPT_trigger(void *data);
void ATMO_BLECharacteristicCrew_INTERRUPT_setup(void *data);
void ATMO_BLECharacteristicCrew_INTERRUPT_setValue(void *data);
void ATMO_BLECharacteristicCrew_INTERRUPT_written(void *data);
void ATMO_BLECharacteristicCrew_INTERRUPT_subscibed(void *data);
void ATMO_BLECharacteristicCrew_INTERRUPT_unsubscribed(void *data);

void ATMO_BLECharacteristicColour_INTERRUPT_trigger(void *data);
void ATMO_BLECharacteristicColour_INTERRUPT_setup(void *data);
void ATMO_BLECharacteristicColour_INTERRUPT_setValue(void *data);
void ATMO_BLECharacteristicColour_INTERRUPT_written(void *data);
void ATMO_BLECharacteristicColour_INTERRUPT_subscibed(void *data);
void ATMO_BLECharacteristicColour_INTERRUPT_unsubscribed(void *data);

void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_trigger(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setup(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setColorPreset(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessLow(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessOff(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessMedium(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBrightnessHigh(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setRedOn(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setGreenOn(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setBlueOn(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setWhiteOn(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_toggleRed(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_toggleGreen(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_toggleBlue(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setOff(void *data);
void ATMO_EmbeddedNxpRpkRgbLed1_INTERRUPT_setColorHex(void *data);


#ifdef __cplusplus
}
#endif

#endif
